Make inputs and outputs folders, if not already present:
     mkdir inputs
     mkdir outputs

Put video files in the inputs folder and run

     python frames_extractor.py

You will get frames for each video file in the outputs folder.

