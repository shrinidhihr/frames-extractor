import sys

sys.path.insert(1, r'C:\Users\hp\Documents\datascience')
from tools import beep

import cv2
import os
from pathlib import Path

for fname in os.listdir("./inputs"):
    print("Processing", fname)
    fname_without_extension = fname.split('.')[0]
    inputFilePath = "./inputs/" + fname
    vidcap = cv2.VideoCapture(inputFilePath)

    success, image = vidcap.read()
    count = 0

    outputFilePath = Path("./outputs", fname_without_extension)
    if not os.path.exists(outputFilePath):
        os.mkdir(outputFilePath)

    while success:
        cv2.imwrite(f"./outputs/{fname_without_extension}/{fname_without_extension}_frame{count}.png", image)
        success, image = vidcap.read()
        count += 1
beep()
