Pyscene-detect splits videos based on scene changes in camera angle/cuts.
https://pyscenedetect.readthedocs.io/en/latest/

# Usage:

## Setup:
Install using https://pyscenedetect.readthedocs.io/en/latest/download/ :

    pip install scenedetect[opencv]

Download/use the ffmpeg and mkvmerge executable files and put them in your venv/scripts folder (where python.exe is found).

# Running:
Cd into experiments folder.
Place input videos in the input folder.
Use in the windows powershell:

    scenedetect -i inputs/filename.mp4 -o output/filename/ detect-content --threshold 27 split-video -hq

    to split a video into different parts based on the camera angle.
    You will have to fine tune using the threshold variable, with values like 30, 27... to get better clips. Lesser the threshold, more the clips generated. 

    -hq option is for high quality.
